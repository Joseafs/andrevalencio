<?php
    include_once("_cabecalho.php");
   $arquivo = $_GET["arquivo"];
   $conn->query("UPDATE DISCOGRAFIAMUSICA SET QT_ACESSO = QT_ACESSO + 1 WHERE ID_DISCOGRAFIA=".$_GET['gal']." AND ID_MUSICA=".$_GET['qt']);
   if(isset($arquivo) && file_exists($arquivo)){
      switch(strtolower(substr(strrchr(basename($arquivo),"."),1))){
		 case "pdf": $tipo="application/pdf"; break;
         case "exe": $tipo="application/octet-stream"; break;
         case "zip": $tipo="application/zip"; break;
         case "doc": $tipo="application/msword"; break;
         case "xls": $tipo="application/vnd.ms-excel"; break;
         case "ppt": $tipo="application/vnd.ms-powerpoint"; break;
         case "gif": $tipo="image/gif"; break;
         case "png": $tipo="image/png"; break;
         case "jpg": $tipo="image/jpg"; break;
         case "mp3": $tipo="audio/mpeg"; break;
         case "php": header("Location:index.php");
         case "htm": header("Location:index.php");
         case "html": header("Location:index.php");
	}
      header("Content-Type: ".$tipo);
      header("Content-Length: ".filesize($arquivo));
      header("Content-Disposition: attachment; filename=".basename($arquivo)); 
      readfile($arquivo);
      exit;
   }
?>