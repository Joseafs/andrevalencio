<?php
    if ($_POST['btnEnviar']=="Enviar") {

        $erro = "";

        if ($nome->isEmpty()) {
            $erro = "Preencha seu Nome";
        } elseif ($email->isEmpty() || !$email->isMail()) {
            $erro = "Informe seu E-mail";
        } elseif ($telefone->isEmpty()) {
            $erro = "Informe o Telefone";
        } elseif ($cidade->isEmpty()) {
            $erro = "Informe o Nome da Cidade";
        } elseif ($estado->isEmpty()) {
            $erro = "Informe o Estado";
        } elseif ($assunto->isEmpty()) {
            $erro = "Informe o Assunto";
        } elseif ($mensagem->isEmpty()) {
            $erro = "Informe uma mensagem";
        }

        if (empty($erro)) {
            $emailConteudo = $conn->query("SELECT * FROM CONTEUDOEMAIL WHERE ID_CONTEUDOEMAIL=1");
            $emailConteudo[0]['CONTEUDO'] = str_replace("%NOME%", $_POST['nome'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%EMAIL%", $_POST['email'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%TELEFONE%", $_POST['telefone'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%CIDADE%", $_POST['cidade'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%ESTADO%", $_POST['estado'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%ASSUNTO%", $_POST['assunto'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%MENSAGEM%", nl2br($_POST['mensagem']), $emailConteudo[0]['CONTEUDO']);

            $enviaremail = new Mail($config->getConfig(1)." - CONTATO VIA WEBSITE", $config->getConfig(12), $emailConteudo[0]['ASSUNTO'], $emailConteudo[0]['CONTEUDO']);
            $enviaremail->setReplyTo($_POST['nome'], $_POST['email']);
            $enviaremail->setTo($config->getConfig(1)." - CONTATO VIA WEBSITE", $config->getConfig(12));
            if ($enviaremail->enviar()) {
                $conn->beginTransaction();
                $sql = "INSERT INTO LOGCONTATO(ID_LOGCONTATO, NM_LOGCONTATO, EMAIL, TELEFONE, CIDADE, ESTADO, ASSUNTO, MENSAGEM) ".
                        "VALUES('',".$nome->getValorSql().",".$email->getValorSql().",".$telefone->getValorSql().",".$cidade->getValorSql().",".$estado->getValorSql().",".$assunto->getValorSql().",".$mensagem->getValorSql().") ";
                $conn->query($sql);
                $conn->endTransaction();

                $_POST = array();
                echo "<script>alert('Sua mensagem foi enviada com sucesso.\\n\\nEntraremos em contato o mais breve possivel.\\n\\nObrigado!')</script>";
            }else{
                echo "<script>alert('Erro no envio do contato.\\n\\nTente novamente!')</script>";
            }
        } else {
            echo "<script>alert('".$erro."')</script>";
        }
    }
  
?>

<div id="fullpage">
    <div id="hm-banner" class="section" >
        <?php
            if(!file_exists($_GET["cod"].".php") || $_GET["cod"]=="" || is_numeric($_GET["cod"]) || $_GET["cod"]=="home"){
                $sql="SELECT B.* FROM BANNER B WHERE B.ID_BANNERPOSICAO=1 AND B.ID_BANNERLOCAL=2 AND B.TP_STATUS='A' AND CURRENT_DATE BETWEEN B.DT_INICIO AND B.DT_FIM ORDER BY RAND() LIMIT 0,4";
                $banner1920x900=$conn->query($sql);
        ?>
                <div class='banner-1366x768 royalSlider rsMinW zInd1 floatL bShwOut-1p'>
                    <?php
                        if(count($banner1920x900)>0){

                            for($i=0 ;$i < count($banner1920x900); $i++){
                    ?>
                                <div class='rsContent'>
                                    <a  title="<?=$banner1920x900[$i]['DS_BANNER']?>" href="<?=(empty($banner1920x900[$i]['LINK'])?"javascript:void(0);":(substr($banner1920x900[$i]['LINK'],0,4)!="http"?"http://".$banner1920x900[$i]['LINK']:$banner1920x900[$i]['LINK']))?>" <?=(!empty($banner1920x900[$i]['LINK'])?"target='".$banner1920x900[$i]['TARGET']."'":"")?> id="banner_<?=$banner1920x900[$i]['ID_BANNER']?>" >
                                        <?php
                                            if (file_exists("./arquivos/banners/".$banner1920x900[$i]['ID_BANNER'].".jpg")) {
                                        ?>
                                        <div class='imgOn' style="background-image:  url(/arquivos/banners/<?=$banner1920x900[$i]['ID_BANNER']?>.jpg); background-position: top center;" title="<?=$banner1920x900[$i]['DS_BANNER']?>"  alt="<?=$banner1920x900[$i]['DS_BANNER']?>"></div>
                                           
                                        <?php } else { ?>
                                            <div class='imgOff bgFirst'></div>
                                        <?php
                                            }
                                        ?>
                                    </a>
                                </div>
                    <?php
                            }
                        } else {
                    ?>
                            <div class='rsContent'>
                                <div class='imgOff bgFirst'></div>
                            </div>
                    <?php
                        }
                    ?>
                </div>
        <?php
            }
        ?>
    </div>
    <div id="hm-biografia" class="section bgOpc-dark8 md-min-hg-0">
        <div id="bg-biografia" class="bgMask imgOn zInd1"></div>
        <div class="bgMask zInd2 bgDark opct4"></div>
        <div class="bgMask zInd9 md-pRelative">
            <div class="content">
                <div class="container pdg30T pdg15L">
                    <h2 class="fSize52 cWhite fLight personTitle tLeft"><span class="fNormal">Bio</span>grafia</h2>
                </div>
                <div class="contTxt min-hg-500p pAbsolute pRight pBottom bgWhite wd-60 floatR">
                    <div class="pRelative hg-100 wd-100">
                        <div class="wd-80 floatL default cGray3 md-wd-100 tLeft">
                             <?php
                                $dadosBiografia=$conn->query("SELECT * FROM INFORMACAO WHERE ID_INFORMACAOLOCAL=2 AND ID_INFORMACAOCATEGORIA=6");
                                echo (strlen(strip_tags(stripslashes($dadosBiografia[0]['DS_INFORMACAO']))) > 1400 ? substr(strip_tags(stripslashes($dadosBiografia[0]['DS_INFORMACAO'])), 0, 1400) . "..." : strip_tags(stripslashes($dadosBiografia[0]['DS_INFORMACAO'])));
                            ?>
                        </div>
                        <div class="btnInfo floatL blop-down tCenter">
                            <a class="pdg8 personLink bgSecond fSize24 fLight cWhite live-3d floatL tUppercase effRipple md-wd-100" href="/biografia" alt="mais" title="mais">
                                Mais
                                <span class='branch live-3d'></span>
                                <span class='branch live-3d'></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="hm-instagram" class="section simpleSct">
        <div class="bgMask bgDark opct8"></div>
        <div class="bgMask zInd9 contSct">
            <div class="container">
                <div class="content pdg30T pdg30B pRelative tCenter">
                    <div class="wd-100 floatL pdg15L">
                        <h2 class="fSize52 cWhite fLight personTitle tLeft"><span class="fNormal">Insta</span>gram</h2>
                    </div>
                    <div class="wd-80 dInlineB default cWhite pdg40T pdg8 pdg20B sm-wd-100">
                        <?php
                            $dadosInstagram=$conn->query("SELECT * FROM INFORMACAO WHERE ID_INFORMACAOLOCAL=1 AND ID_INFORMACAOCATEGORIA=2");
                            echo (strlen(strip_tags(stripslashes($dadosInstagram[0]['DS_INFORMACAO']))) > 600 ? substr(strip_tags(stripslashes($dadosInstagram[0]['DS_INFORMACAO'])), 0, 600) . "..." : strip_tags(stripslashes($dadosInstagram[0]['DS_INFORMACAO'])));
                        ?>
                    </div>
                    <!-- SnapWidget -->
                    <script src="https://snapwidget.com/js/snapwidget.js"></script>
                    <iframe class="wd-100 pdg8 floatL pdg20L snapwidget-widget md-displayOff" src="https://snapwidget.com/embed/code/198611" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;"></iframe>
                    
                    <iframe class="snapwidget-widget wd-100 pdg8 floatL displayOff md-displayOn" src="https://snapwidget.com/embed/263975"  allowTransparency="true" frameborder="0" scrolling="no"></iframe>
                    <div class="floatL wd-100 pdg30T pdg8 tCenter">
                        <a class="pdg8 personLink bgSecond fSize24 fLight cWhite live-3d dInlineB tUppercase effRipple md-wd-100" target="_blank" rel='nofollow' href="<?=(empty($config->getConfig(9)) ? "javascript:void(0);" : (substr($config->getConfig(9), 0, 4) != "http" ? "http://".$config->getConfig(9) : $config->getConfig(9)))?>" title="Ver mais">
                            Ver Mais
                            <span class='branch live-3d'></span>
                            <span class='branch live-3d'></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="hm-videos" class="section simpleSct">
        <div class="bgMask imgOn zInd1" style="background-image: url(/img/bg-videos01.jpg);  background-position:bottom 120px center;"></div>
        <div class="bgMask bgDark opct7 zInd2"></div>
        <div class="bgMask zInd9 contSct">
            <div class="container">
                <div class="content pdg30T pRelative tCenter zInd2">
                    <div class="wd-100 floatL pdg15L">
                        <h2 class="fSize52 cWhite fLight personTitle tLeft">V�deos</h2>
                    </div>
                    <div class="wd-70 dInlineB sm-wd-100 live-5d pdg30T">
                        <div class="wd-100 floatL ifmPlayer-cont displayOff pdg20T pdg3">
                            <div class="ifmPlayer"></div>
                        </div>
                        <?php
                            $dadosPlayer = $conn->query("SELECT * FROM VIDEO WHERE BO_ATIVO='S' AND ID_VIDEOCATEGORIA=2 ORDER BY ID_VIDEO DESC LIMIT 0,1");
                            $dadosVideo = $conn->query("SELECT * FROM VIDEO WHERE BO_ATIVO='S' AND ID_VIDEOCATEGORIA=2 ORDER BY ID_VIDEO DESC LIMIT 0,9");

                                if(count($dadosVideo)>0){
                        ?>
                                    <div class="view-slick4 floatL wd-100">
                                        <?php 
                                            if(!empty($dadosPlayer)){
                                                
                                                $vdURL= '';
                                                $vdIMG= '';

                                                if ($dadosPlayer[0]['TP_ORIGEM'] == "Y") {
                                                    parse_str(parse_url($dadosPlayer[0]['URL'], PHP_URL_QUERY), $url);

                                                    $vdURL= $url['v'];
                                                    $vdIMG= 'http://img.youtube.com/vi/'.$url['v'].'/mqdefault.jpg';

                                                } elseif ($dadosPlayer[0]['TP_ORIGEM'] == "V") {

                                                    $vimeo = explode("/", $dadosPlayer[0]['URL']);

                                                    $url_imagem = parse_url($dadosPlayer[0]['URL']);

                                                        if($url == 'www.vimeo.com' || $url == 'vimeo.com'){
                                                            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video".$url_imagem['path'].".php"));
                                                        }

                                                    $vdURL= $vimeo[3];

                                                    $vdIMG= imagemVimeo('http://vimeo.com'.$url_imagem['path'], 'thumbnail_large')."' id='video_".$dados[0]['ID_VIDEO'];
                                                }
                                            }
                                            for ($i = 0; $i < count($dadosVideo); $i++) {
                                                
                                                $playerOrdem ='';
                                                        
                                                //parse_str(parse_url($dadosVideo[$i]['URL'], PHP_URL_QUERY), $url);
                                                
                                                $vdURL= '';
                                                $vdIMG= '';

                                                if ($dadosVideo[$i]['TP_ORIGEM'] == "Y") {
                                                    parse_str(parse_url($dadosVideo[$i]['URL'], PHP_URL_QUERY), $url);

                                                    $vdURL= $url['v'];
                                                    $vdIMG= 'http://img.youtube.com/vi/'.$url['v'].'/mqdefault.jpg';

                                                } elseif ($dadosVideo[$i]['TP_ORIGEM'] == "V") {

                                                    $vimeo = explode("/", $dadosVideo[$i]['URL']);

                                                    $url_imagem = parse_url($dadosVideo[$i]['URL']);

                                                        if($url == 'www.vimeo.com' || $url == 'vimeo.com'){
                                                            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video".$url_imagem['path'].".php"));
                                                        }

                                                    $vdURL= $vimeo[3];

                                                    $vdIMG= imagemVimeo('http://vimeo.com'.$url_imagem['path'], 'thumbnail_large')."' id='video_".$dados[$i]['ID_VIDEO'];
                                                }
                                                
                                                if($i == 0){
                                                    $playerOrder= "id='startPlayer'";
                                                }
                                        ?>
                                                <div id='ifmPlayer-<?=$i?>' class="pdg8 ">
                                                    <a <?=$playerOrder?> class="contLimit effRipple dInlineB effZoom bRad3 ovflwH pRelative max-wd-100 linkNull" data-descricao='' data-sect='Home' data-type='<?=$dadosVideo[$i]['TP_ORIGEM']?>' data-url='<?=$vdURL?>' data-ref='ifmPlayer-<?=$i?>' onclick="ifmPlayer(this)" title="<?=$dadosVideo[$i]['NM_VIDEO']?>" href="#" >
                                                        <div class='imgOn spy-child live-3d' style='background-image: url(<?=$vdIMG?>);'  alt='<?=$dadosVideo[$i]['NM_VIDEO']?>' ></div>
                                                        
                                                        <div class="zInd1 wd-100 floatL pdg3 cSecond fSize14 fNormal tLeft bgWhite pRelative"> 
                                                            <?=(strlen($dadosVideo[$i]['NM_VIDEO']) > 35 ? substr($dadosVideo[$i]['NM_VIDEO'], 0, 35)."..." : $dadosVideo[$i]['NM_VIDEO'])?>
                                                        </div>
                                                    </a>
                                                </div>
                                        <?php
                                            }
                                        ?>
                                    </div>
                        <?php
                            }else{
                                echo "<div class='fSize16 pdg8 cGray3 fBold'>Nenhum registro cadastrado.</div>";
                            }
                        ?>
                        <div class="floatL wd-100 pdg8 tCenter">
                            <a class="pdg8 personLink bgSecond fSize24 fLight cWhite live-3d dInlineB tUppercase effRipple md-wd-100" href="/midia" title="Ver mais">
                                Ver Mais
                                <span class='branch live-3d'></span>
                                <span class='branch live-3d'></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class='blop-down pLeft pRight min-hg-150p bgWhite zInd3'></div>
    </div>
    <div id="hm-musicas" class="section simpleSct">
        <div class="bgMask zInd9 contSct">
            <div class="container">
                <div class="content pdg30T pRelative tCenter zInd2">
                    <div class="wd-100 floatL pdg15L">
                        <h2 class="fSize52 cThird fNormal personTitle tLeft">M�sicas</h2>
                    </div>
                    <div class="wd-100 floatL mgn20T">
                        <?php
                            $sql = "SELECT * FROM DISCOGRAFIA WHERE ID_DISCOGRAFIACATEGORIA=2 AND BO_ATIVO='S' ORDER BY ID_DISCOGRAFIA DESC";

                            $dadosAlbum = $conn->query($sql);

                            $unLink =  "<a class='tb-col-child tb-child-link effRipple effShadow live-3d linkNull' href='#' alt='Invalido' title='Invalido' target='_blank' rel='nofollow'></a>";

                            if(count($dadosAlbum)>0){

                                for ($j = 0; $j < count($dadosAlbum); $j++) {

                                    $sqlMusica = "SELECT * FROM DISCOGRAFIAMUSICA WHERE ID_DISCOGRAFIA=".$dadosAlbum[$j]['ID_DISCOGRAFIA'];
                                    $dadosMusica = $conn->query($sqlMusica);

                                    if(count($dadosMusica)>0){
                        ?>
                                        <div class="floatL wd-100 pdg8 mgn20B">
                                            <div class="content pRelative min-hg-300p">
                                                <div class="wd-30 hg-100 floatL pAbsolute pLeft pTop tCenter pdg3T pdg3R sm-pdg5 md-pRelative md-wd-100 tCenter">
                                                    <div class="capa_album dInlineB wd-100 hg-100 pRelative">
                                                        <?php if(file_exists("./arquivos/capadisco/".$dadosAlbum[$j]['ID_DISCOGRAFIA'].".jpg")){ ?>
                                                            <div class="bgMask imgOn zInd9" style="background-image: url(/arquivos/capadisco/<?=$dadosAlbum[$j]['ID_DISCOGRAFIA']?>.jpg);"></div>
                                                        <?php }else{ ?>
                                                            <img class="bgMask imgOff" alt="<?=$dadosAlbum[$j]['NM_DISCO']?>" />
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="wd-70 md-wd-100 tb-view floatR ovflwA-6p list-music">
                                                    <?php

                                                            for ($i = 0; $i < count($dadosMusica); $i++) {
                                                    ?>
                                                                <div class='tb-line mgn3T bgOpc-dark9 pRelative sm-pdg0'>
                                                                    <div class='tb-col wd-45 '>
                                                                        <div class='tb-col-child fBold tLeft sm-tCenter'>
                                                                            <div class="pdg60L sm-pdg5 fNormal cWhite floatL wd-100">
                                                                                <span class="fSize22 floatL clearB" ><?=(strlen($dadosMusica[$i]['NM_MUSICA']) > 20 ? substr($dadosMusica[$i]['NM_MUSICA'], 0, 20)."..." : $dadosMusica[$i]['NM_MUSICA'])?></span>
                                                                                <span class="fSize14 floatL clearB" ><?=(strlen($dadosMusica[$i]['URL_LETRA']) > 20 ? substr($dadosMusica[$i]['URL_LETRA'], 0, 20)."..." : $dadosMusica[$i]['URL_LETRA'])?></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <audio id="player_<?=$i?>" class="likePlayer" src="/musicas/<?=$dadosAlbum[$j]['PASTA'].'/'.$dadosMusica[$i]['NM_ARQUIVO']?>" type="audio/mp3" controls="controls"></audio>
                                                                    <div class='floatL sm-wd-100 pAbsolute pRight pTop pBottom sm-pRelative'>
                                                                        <a class="md-wd-100 hg-100 floatL tCenter effShadow-white effRipple pdg10 linkNull dMiddle op-live-3d sm-dInlineB" href="<?=(strlen($dadosMusica[$i]['NM_MUSICA']) > 30 ? substr($dadosMusica[$i]['NM_MUSICA'], 0, 30)."..." : $dadosMusica[$i]['NM_MUSICA'])?>" <?= "onclick='location.href=\"downloadArq.php?qt=".$dadosMusica[$i]['ID_MUSICA']."&gal=".$dadosMusica[$i]['ID_DISCOGRAFIA']."&arquivo=arquivos/musicas/{$dadosAlbum[$j]['PASTA']}/{$dadosMusica[$i]['NM_ARQUIVO']}\" '"?> target="_blank" rel="nofollow" alt="Fazer download" title="Fazer download">
                                                                            <div class="bgMask bgWhite opct1 zInd1 displayOff sm-displayOn"></div>
                                                                            <i class="fIcon-det-download dInlineB fSize28 cFirst zInd2 sm-pdg5B"></i>

                                                                            <div class="blop-down tCenter pdg3B">
                                                                                <span class="fSize12 dInlineB cWhite" ><?=$dadosMusica[$i]['QT_ACESSO']?></span>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                    <?php
                                                            }
                                                    ?>
                                                </div>
                                            </div>    
                                        </div>    
                        <?php
                                    }
                                }
                            }
                        ?>
                    </div>
                    <!--
                    <div class="floatL wd-100 pdg30T pdg8 tCenter">
                        <a class="pdg8 personLink bgSecond fSize24 fLight cWhite live-3d dInlineB effRipple md-wd-100" href="<?=(empty($config->getConfig(9)) ? "javascript:void(0);" : (substr($config->getConfig(9), 0, 4) != "http" ? "http://".$config->getConfig(9) : $config->getConfig(9)))?>" title="Discografia">
                            Discografia
                            <span class='branch live-3d'></span>
                            <span class='branch live-3d'></span>
                        </a>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>
    <div id="hm-contato" class="section simpleSct">
        <div class="bgMask bgDark opct1"></div>
        <div class="bgMask zInd9 contSct">
            <div class="container">
                <div class="content pdg30T pRelative tCenter zInd2">
                    <div class="wd-100 floatL pdg15L">
                        <h2 class="fSize52 cThird fNormal personTitle tLeft">Contato</h2>
                    </div>
                    <div class="wd-100 floatL mgn30T pdg8" >
                        <form class="formInfo floatL wd-100 ipt-pColor-dark3 fLight" method="POST" action="" >
                            <div class="inputField wd-50 pdg3">
                                <input id="iptNome" type="text" placeholder="Nome" name="nome" class="selectField bgFirst fSize18 bShwOut-1p" required>
                            </div>
                            <div class="inputField wd-50 pdg3">
                                <input id="iptEmail" type="email" placeholder="E-mail" name="email" class="selectField bgFirst fSize18 bShwOut-1p" required>
                            </div>
                            <div class="inputField wd-33 pdg3">
                                <input id="iptFone" type="tel" placeholder="Telefone" name="telefone" class="selectField bgFirst fSize18 bShwOut-1p fone" required>
                            </div>
                            <div class="inputField wd-33 pdg3">
                                <input id="iptCidade" type="text" placeholder="Cidade" name="cidade" class="selectField bgFirst fSize18 bShwOut-1p" required>
                            </div>
                            <div class="inputField wd-33 pdg3">
                                <select id="sltEstado" name="estado" class="selectField bgFirst fSize18 cThird bShwOut-1p" required>
                                    <option value="" class="displayOff cThird">Estado</option>
                                    <?php
                                        $estados = $conn->query("SELECT * FROM ESTADO ORDER BY DS_ESTADO");
                                        for ($i = 0; $i < count($estados); $i++) {
                                            echo "<option class='fSize18' value='".$estados[$i]['ID_ESTADO']."'>".$estados[$i]['DS_ESTADO']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="inputField wd-100 pdg3">
                                <input id="iptAssunto" type="text" placeholder="Assunto" name="assunto" class="selectField bgFirst fSize18 bShwOut-1p" required>
                            </div>
                            
                            <div class="inputField wd-100 pdg3" >
                                <textarea class="selectField min-hg-100p bgFirst fSize18 bShwOut-1p" id="txtMessage" placeholder="Mensagem" name="mensagem" ></textarea>
                            </div>
                            <div class='wd-100 floatL pdg3'>
                                <button name="btnEnviar" value="Enviar" class="personButton cWhite bgThird effRipple effShadow pntPointer pRelative op-live-3d wd-100 bgGreen floatL bShwOut-1p" type="submit" >
                                    <span class='fLato fSize22 fLight tUppercase pdg15T pdg15B dInlineB'>Enviar</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<script>
    window.onload = function() {
        ifmPlayer($("#startPlayer"));
    };
</script>