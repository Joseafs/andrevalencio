<?php
    $idGaleria = $_GET['id'];

    if(is_numeric($idGaleria) && $idGaleria > 0){
        $sql = "SELECT * FROM GALERIA WHERE ID_GALERIA={$idGaleria}";
        $galeriaDet = $conn->query($sql);

        if(!empty($galeriaDet)){
?>
            <div id="galeria" class="wd-100 floatL">
                <div class="container">
                    <div class="content">
                        <div class="wd-100 floatL pdg15L">
                            <h2 class="fSize52 cGray3 fLight personTitle tLeft"><?=$galeriaDet[0]['NM_GALERIA']?></h2>
                        </div>
                        <?php
                            if(!empty($galeriaDet[0]['DS_GALERIA'])){
                        ?>   
                                <div class="default floatL wd-100 pdg15 fSize20 cGray3">
                                    <?=stripslashes($galeriaDet[0]['DS_GALERIA'])?>
                                </div>
                        <?php
                            }
                        ?>
                        <div class="content pdg30T pdg30B pdg3">
                            <div class="wd-100 floatL contBox tCenter min-hg-350p">
                                <?php
                                    $fotos = $conn->query("SELECT * FROM GALERIAFOTO WHERE ID_GALERIA={$idGaleria}");
                                    if(count($fotos)>0){
                                        for ($i = 0; $i < count($fotos); $i++){
                                ?>
                                            <div class='wd-20 md-wd-33 sm-wd-50 xsm-wd-100'>    
                                                
                                                <a class="contLimit dInlineB effRipple effShadow ovflwH effZoom view-iframe live-3d min-hg-200p wd-100 bShwB"  href="/foto/<?=str_replace(" ", "%20", $galeriaDet[0]['PASTA']).'/'.str_replace(" ", "%20", $fotos[$i]['NM_ARQUIVO'])?>" rel="<?=$fotos[$i]['ID_GALERIA']?>" title="<?=$fotos[$i]['DS_GALERIAFOTO']?>" >
                                                    <div class="bgMask imgOn spy-child live-3d" style="background-image: url('/miniatura/<?= str_replace(" ", "%20", $galeriaDet[0]['PASTA']).'/'.str_replace(" ", "%20", $fotos[$i]['NM_ARQUIVO'])?>');"  ></div>
                                                </a>
                                            </div>
                                <?php      
                                        }
                                    } else {
                                        echo "<div class='fSize16 cGray3 pdg8 pdg30T pdg30B wd-100 tCenter'>Nenhuma foto cadastrada.</div>";
                                    } 
                                ?>
                            </div>
                            <a class="floatR bgOpc-dark1 cGray3 pdg15 pdg20R pdg20L fSize16 effRipple effShadow live-3d md-wd-100 tCenter mgn20T" href="/midia" alt="Conferir outros �lbums" title="Conferir outros �lbums" >Conferir outros �lbums</a>
                        </div>
                    </div>
                </div>
            </div>
<?php
        }
    }
?>