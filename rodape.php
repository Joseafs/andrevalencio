<footer class="tCenter zInd9 live-5d pAbsolute md-pRelative">
    <div class='wd-100 floatL pdg3 pRelative'>
        <div class='bgMask zInd1 bgThird'></div>
        <div class='eff-wings bgMask'>
            <div class='arm-child'><div class='finger bgThird'></div></div>
            <div class='arm-child'><div class='finger bgThird'></div></div>
        </div>
        <div class='wd-100 floatL pdg8 zInd2 pRelative'>
            <div class="container">
                <div class='content pdg3 pRelative contBox'>
                    <div class='wd-33 md-wd-50 sm-wd-100 sm-pdg10'>
                        <h2 class="cWhite tLeft fNormal fSize28 sm-tCenter">
                            Facebook
                        </h2>
                        <div class='fb-content max-wd-100 dInlineB'>
                            <div class="fb-page" data-href="https://www.facebook.com/andrevalencio" data-tabs="timeline" data-height="150" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/andrevalencio" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/andrevalencio">Andr� Val�ncio</a></blockquote></div>
                        </div>
                    </div>
                    <div class='wd-33 md-wd-50 sm-wd-100 sm-pdg10'>
                        <h2 class="cWhite fNormal fSize28">
                            Redes Sociais
                        </h2>
                        <div class='wd-100 floatL tCenter pdg8'>
                            <div class="dInlineB pdg8">
                                <?php
                                    if($config->getConfig(6)!= ""){
                                ?>
                                        <a class="effRipple hvr-buzz-out mgn5" title="Facebook" href="<?=(empty($config->getConfig(6)) ? "javascript:void(0);" : (substr($config->getConfig(6), 0, 4) != "http" ? "http://".$config->getConfig(6) : $config->getConfig(6)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-facebook.svg" alt="Facebook"/>
                                        </a>
                                <?php
                                    } if($config->getConfig(7)!= "") {
                                ?>
                                        <a class="effRipple hvr-buzz-out mgn5" title="Twitter" href="<?=(empty($config->getConfig(7)) ? "javascript:void(0);" : (substr($config->getConfig(7), 0, 4) != "http" ? "http://".$config->getConfig(7) : $config->getConfig(7)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-twitter.svg" alt="Twitter"/>
                                        </a>
                                <?php
                                    } if($config->getConfig(8)!= "") {
                                ?>  
                                        <a class="effRipple hvr-buzz-out mgn5" title="Youtube" href="<?=(empty($config->getConfig(8)) ? "javascript:void(0);" : (substr($config->getConfig(8), 0, 4) != "http" ? "http://".$config->getConfig(8) : $config->getConfig(8)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-youtube.svg" alt="Youtube"/>
                                        </a>
                                <?php
                                    } if($config->getConfig(9)!= "") {
                                ?>
                                        <a class="effRipple hvr-buzz-out mgn5" title="Instagram"  href="<?=(empty($config->getConfig(9)) ? "javascript:void(0);" : (substr($config->getConfig(9), 0, 4) != "http" ? "http://".$config->getConfig(9) : $config->getConfig(9)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-instagram.svg" alt="Instagram" />
                                        </a>
                                <?php
                                    } if($config->getConfig(10)!= "") {
                                ?>
                                        <a class="effRipple hvr-buzz-out mgn5" title="Soundcloud" href="<?=(empty($config->getConfig(10)) ? "javascript:void(0);" : (substr($config->getConfig(10), 0, 4) != "http" ? "http://".$config->getConfig(10) : $config->getConfig(10)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-soundcloud.svg" alt="Soundcloud"/>
                                        </a>
                                <?php
                                    } if($config->getConfig(11)!= "") {
                                ?>
                                        <a class="effRipple hvr-buzz-out mgn5" title="Palco MP3" href="<?=(empty($config->getConfig(11)) ? "javascript:void(0);" : (substr($config->getConfig(11), 0, 4) != "http" ? "http://".$config->getConfig(11) : $config->getConfig(11)))?>" target="_blank" rel="nofollow">
                                            <img class="floatL" width="42" src="/img/icon-lg-palcoMp3.svg" alt="Palco MP3"/>
                                        </a>
                                <?php
                                    }
                                ?>
                           </div>
                        </div>
                    </div>
                    <div class='wd-33 md-wd-100 tCenter sm-pdg10'>
                        <div class="dInlineB tLeft">
                            <h2 class="cWhite fNormal fSize28">
                                Contato
                            </h2>
                            <?php
                                if($config->getConfig(4)!= ""){
                            ?>
                                <a class="fSize16 floatL fBold cWhite effShadow-white effRipple pdg8 ovflwH live-3d clearB bRad3"  href="tel: <?=$config->getConfig(4)?>" >
                                    <span class="fIcon floatL mgn5R">phone_in_talk</span>
                                    <?=$config->getConfig(4)?>
                                </a>
                            <?php
                                } if($config->getConfig(5)!= ""){
                            ?>
                                    <div class="fSize16 cWhite floatL clearB pdg8L"><?=$config->getConfig(5)?></div>
                            <?php
                                } 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wd-100 floatL bgFirst">
        <div class="container">
            <div class="wd-100 dMiddle sm-dInlineB">
                <div class="wd-66 floatL pdg8 fSize16 cWhite tLeft fLight sm-wd-100 sm-tCenter">
                    Copyright 2016 - Todos os direitos reservados
                </div>
                <div class="wd-33 floatL tRight sm-wd-100 sm-tCenter">
                    <a class="effZoom effRotate ovflwV dInlineB pdg8 effOpac6 dMiddle" href="http://www.uses.com.br/"  title="Uses - Software e Design" rel="follow" target="_blank">
                        <img class="floatL max-wd-100 spy-child6x dizzy-child live-3d mgn5R puny-child" src="/img/lgUses-symbol.svg" width="30" alt="Uses - Software e Design"/>
                        <img class="floatL max-wd-100 spy-child live-3d puny-child" src="/img/lgUses-txt.svg" width="55" alt="Uses - Software e Design" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>