<div id="midias" class="wd-100 floatL">
    <div class="container">
        <div class="content">
            <div class="wd-100 floatL pdg15L">
                <h2 class="fSize52 cGray3 fLight personTitle tLeft"><span class="fNormal">V</span>�deos</h2>
            </div>
            <?php
                $dadosSecao=$conn->query("SELECT DS_INFORMACAO FROM INFORMACAO WHERE ID_INFORMACAOLOCAL=1 AND ID_INFORMACAOCATEGORIA=3");
            
                if(!empty($dadosSecao[0]['DS_INFORMACAO'])){
            ?>   
                    <div class="default floatL wd-100 pdg15 fSize20 cGray3">
                        <?=stripslashes($dadosSecao[0]['DS_INFORMACAO'])?>
                    </div>
            <?php
                }
            ?>
        
            <div class="wd-100 floatL ifmPlayer-cont displayOff pdg20T pdg3">
                <div id="vdPlayer" class="ifmPlayer pRelative"></div>
                <div id='ifmTxt' class='floatL wd-100 pdg8 cGray3 fSize16'></div>
            </div>
            
            <div class="contBox wd-100 floatL tCenter pdg20T pdg20B">
                <?php 
                    $dadosPgVideo = $conn->query("SELECT * FROM VIDEO WHERE BO_ATIVO='S' AND ID_VIDEOCATEGORIA IN (1,2) ORDER BY ID_VIDEO DESC");

                    if (count($dadosPgVideo) > 0) {
                        for ($i = 0; $i < count($dadosPgVideo); $i++) {
                            
                            $vdURL= '';
                            $vdIMG= '';
                            
                            if ($dadosPgVideo[$i]['TP_ORIGEM'] == "Y") {
                                parse_str(parse_url($dadosPgVideo[$i]['URL'], PHP_URL_QUERY), $url);
                                
                                $vdURL= $url['v'];
                                $vdIMG= 'http://img.youtube.com/vi/'.$url['v'].'/mqdefault.jpg';

                                
                            }elseif ($dadosPgVideo[$i]['TP_ORIGEM'] == "V") {

                                $vimeo = explode("/", $dadosPgVideo[$i]['URL']);

                                $url_imagem = parse_url($dadosPgVideo[$i]['URL']);
                                
                                    if($url == 'www.vimeo.com' || $url == 'vimeo.com'){
                                        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video".$url_imagem['path'].".php"));
                                    }

                                $vdURL= $vimeo[3];

                                $vdIMG= imagemVimeo('http://vimeo.com'.$url_imagem['path'], 'thumbnail_large')."' id='video_".$dados[$i]['ID_VIDEO'];
                            } 					
                            /* list($ano, $mes, $dia) = explode("-", $dadosPgVideo[$i]['DH_CADASTRO']);  */
                ?>
                            <div id='ifmPlayer-<?=$i?>' class='wd-33 floatL md-wd-50 xsm-wd-100 pdg3'>
                                
                                <a class="contLimit effRipple effShadow effZoom dInlineB wd-100 pRelative live-3d tCenter min-hg-250p bgOpc-dark1 bShwB linkNull" data-descricao='<?=$dadosPgVideo[$i]['DS_VIDEO']?>' data-target='mPlay' data-sect='secao' data-type='<?=$dadosPgVideo[$i]['TP_ORIGEM']?>' data-url='<?=$vdURL?>' data-ref='ifmPlayer-<?=$i?>' onclick="ifmPlayer(this)" title="<?=$dadosPgVideo[$i]['NM_VIDEO']?>" href="#">
                                    <div class='bgMask imgOn spy-child live-3d zInd1' style='background-image: url(<?=$vdIMG?>);'  alt='<?=$dadosPgVideo[$i]['NM_VIDEO']?>' ></div>
                                    <div class="floatL wd-100 blop-down bgOpc-white9 live-3d zInd2">
                                        <div class="pdg8 floatL wd-100 default tCenter">
                                            <span class="fSize16 fBold">
                                                <?=(strlen($dadosPgVideo[$i]['NM_VIDEO']) > 35 ? substr($dadosPgVideo[$i]['NM_VIDEO'], 0, 35)."..." : $dadosPgVideo[$i]['NM_VIDEO'])?>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                <?php          
                        }
                    }
                ?>
            </div>
            
            <div class="wd-100 floatL pdg15L">
                <h2 class="fSize52 cGray3 fLight personTitle tLeft"><span class="fNormal">�lbuns</span> de Fotos</h2>
            </div>
            <div class="contBox wd-100 floatL tCenter pdg20T">
                <?php 
                    $dadosGaleria = $conn->query("SELECT * FROM GALERIA WHERE ID_GALERIACATEGORIA=1 ORDER BY DT_GALERIA DESC");

                    if (count($dadosGaleria) > 0) {
                        for ($i = 0; $i < count($dadosGaleria); $i++) {
                            list($ano, $mes, $dia) = explode("-", $dadosGaleria[$i]['DT_GALERIA']);      
                ?>
                            <div class='wd-33 floatL md-wd-50 xsm-wd-100 pdg3'>
                                <a class="contLimit effRipple effShadow effZoom dInlineB wd-100 pRelative live-3d tCenter min-hg-250p bgOpc-dark1 bShwB" title="<?=$dadosGaleria[$i]['NM_GALERIA']?>" href="<?= Link::getLink("fotos", array($dadosGaleria[$i]['ID_GALERIA'], Link::getStringBarra(str_replace("%", " porcento", $dadosGaleria[$i]['NM_GALERIA']))))?>">
                <?php 
                                    if (file_exists("./arquivos/fotodestaque/".$dadosGaleria[$i]['ID_GALERIA'].".jpg")) {
                ?>
                                        <div class='bgMask imgOn spy-child live-3d' style='background-image: url(./arquivos/fotodestaque/<?= $dadosGaleria[$i]['ID_GALERIA']?>.jpg);'  alt='<?=$dadosGaleria[$i]['NM_GALERIA']?>' ></div>

                <?php               }else{ ?>
                                        <div class='bgMask imgOff spy-child live-3d' alt='<?=$dadosGaleria[$i]['NM_GALERIA']?>'  ></div>
                <?php               } ?>
                                    <div class="floatL wd-100 blop-down bgOpc-white9 live-3d">
                                        <div class="pdg5 floatL wd-100 default tLeft">
                                            <span class="fSize14">
                                                <?= strftime("%d/%b/%Y", strtotime($dadosGaleria[$i]['DT_GALERIA'])) ?>
                                            </span>
                                            <br />
                                            <span class="fSize16 fBold">
                                                <?=(strlen($dadosGaleria[$i]['NM_GALERIA']) > 35 ? substr($dadosGaleria[$i]['NM_GALERIA'], 0, 35)."..." : $dadosGaleria[$i]['NM_GALERIA'])?>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                <?php          
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</div>