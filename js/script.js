/* -------------------------------------------------------------------------- */
/* Menu Marker */

var wdWidth = $(window).innerWidth();
var hgWidth = $(window).innerWidth();



//var windHeight = $(window).innerHeight();

/*
$('body').on('scrollTop', 'resize' , function(){
    if (($(window).innerHeight() + $(window).scrollTop()) >= $("body").height()) {
        alert('Birrl');
    }
});
*/

var markWid, markPos, toWid, toPos;
$('.branchMenu').mouseenter(function(){

    toWid = $(this).width();
    toPos = $(this).position();
    /*
    console.log("Width anterior:"+markWid+"   Left anterior:"+markPos.left);
    console.log("W:"+toWid+"L:"+toPos.left);
    */  
    runMarker(toWid, toPos.left);
});

function runMarker(wid, left){
    
    if(wdWidth > 1024){
        wdMark = wid;
        goMark = left;

        $('#linkMarker').css({'width': wdMark, 'left': goMark});
        //$('#linkMarker').css({'width': wdMark, 'left': goMark.left, 'top': goMark.top + 55});
    }else{
        $('#linkMarker').css({'width': markWid, 'left': markPos.left});
    }
}

function localizeMarker(codSessao){

    if(codSessao  === 'biografia'){
        markWid = $("#rf-biografia").width();
        markPos = $("#rf-biografia").position();
    } else if(codSessao  === 'midia'){ 
        markWid = $("#rf-midia").width();
        markPos = $("#rf-midia").position();
    } else if(codSessao  === 'musicas'){ 
        markWid = $("#rf-musicas").width();
        markPos = $("#rf-musicas").position();
    } else if(codSessao  === 'contato'){ 
        markWid = $("#rf-contato").width();
        markPos = $("#rf-contato").position();
    } else{ 
        markWid = 0;
        markPos = 0;
    }
    
    runMarker(markWid, markPos.left);
    
    $('.branchMenu').mouseleave(function(){
        runMarker(markWid, markPos.left);
    });
}

/* -------------------------------------------------------------------------- */
$("#btnScrollTop").click(function() {
    $('html, body').animate({scrollTop: 0}, 1500);
});

$('[class*="effRipple"]').materialripple();

$('.btnActv').click(function(e) {
    e.preventDefault();
    $(this).addClass('active');
});
/* -------------------------------------------------------------------------- */
/*
$("#aside-toggle").click(function(){
    $("#aside-toggle, #aside-toggle .effToggle, #aside-evolve").toggleClass('active');
    $("#header-lg").toggleClass('opct0');
    $("body").toggleClass('blocked');
});
*/
$("#hd-toggle, header ~ .wrapShadow").click(function(){
    $("header, #hd-toggle .effToggle").toggleClass('active');
    $("body").toggleClass('blocked');

    //$("#header-acd-menu").removeClass('starter');
    //$("#header-acd-content").slideToggle();
    //$("body").toggleClass('blocked');

});
/*
$(window).scroll(function(){
    var h = $("header").height();
    var y = $(window).scrollTop();
    if (y > (h)){
        $("header, section").addClass('update');
        $("#header-acd-menu").addClass('starter');
    } else {
        $("header, section").removeClass('update');
    }
});
*/
/* -------------------------------------------------------------------------- */
$(".btnModal, .modal .wrapShadow, .closeModal").click(function(){
    
    var modal = $(this).data('target');
    if($(modal).hasClass('active')) {
        
        $(modal).fadeOut().removeClass('active');
        $("body").removeClass('modal-open');
    }else {
        $('.modal').fadeOut().removeClass('active');
        
        // Add active class to sct title
        $(modal).addClass('active').fadeIn();
        $("body").addClass('modal-open');
    }
});

/* -------------------------------------------------------------------------- */
/* Form */  

$("input.data").mask("99/99/9999");
$("input.cep").mask("99999-999");
$("input.n_casa").mask("9999999");
$("input.rg").mask("99.999.999.9");
$("input.fone").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');



/* -------------------------------------------------------------------------- */
/* Blinds */
$(".clickOff").bind("contextmenu", function(e) {
    e.preventDefault();
});

$("a.linkNull").click(function() {
    return false;
});

/* -------------------------------------------------------------------------- */
function close_accordin_section() {
    $('a.acd-sct-title, a.acd-title-icon').removeClass('active');
    $('.acd-sct-content').slideUp(300).removeClass('open');
}
$('a.acd-sct-title, a.acd-title-icon').click(function(e) {
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');
    if($(e.target).is('.active')) {
        close_accordin_section();
    }else {
        close_accordin_section();
        // Add active class to sct title
        $(this).addClass('active');
        // Open up the hidden content panel
        $(".acd " + currentAttrValue).slideDown(300).addClass('open');
    }
    e.preventDefault();
});
/* -------------------------------------------------------------------------- */
/* slick 1 */
$('.view-slick1').slick({
    adaptiveHeight: true,
    speed: 500,
    dots: true,
    //autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});
$('.view-slick4').slick({
    infinite: true,
    adaptiveHeight: false,
    speed: 300,
    //autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 510,
        settings: {
          slidesToShow: 1
        }
      }
    ]
});


$('.view-slick5').slick({
    infinite: false,
    adaptiveHeight: true,
    speed: 300,
    autoplay: true,
    arrows: true,
    slidesToShow: 5,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1
        }
      }
    ]
});

/* -------------------------------------------------------------------------- */

$(".likePlayer").mediaelementplayer({
    pauseOtherPlayers: true,
    enableKeyboard: true,
    features: ['playpause','current','progress','duration'],

});





// To access player after its creation through jQuery use:

/* -------------------------------------------------------------------------- */
/* Video */
function ifmPlayer(ident) {
    
    var ifrm = document.createElement("iframe");
    var ifrm_desc = document.createElement("div");
    
    var pUrl, pType, pRef, pSect, pDesc, error;
    
    pUrl= $(ident).data('url');
    pType= $(ident).data('type');
    pRef= $(ident).data('ref');
    pSect= $(ident).data('sect');
    pDesc= $(ident).data('descricao');
    
    
    error = true;
    
    if(pType === "Y"){

        ifrm.setAttribute("src", 'https://www.youtube.com/embed/'+pUrl);
        
    } else if(pType === "V") {

        ifrm.setAttribute("src", 'http://player.vimeo.com/video/'+pUrl);
        ifrm.setAttribute("webkitallowfullscreen", '');
        ifrm.setAttribute("mozallowfullscreen", '');
        ifrm.setAttribute("allowfullscreen", '');
        
    } else{
        error = false;
        
        console.log("ERRO !!");
    }
            
    if(error){
        //console.log(pRef);

        ifrm.setAttribute("class", "wd-100 ifmPlayer floatL min-hg-450p");

        $(".ifmPlayer").replaceWith(ifrm);
   
        $("[id*='ifmPlayer-']").removeClass('active');
        $("#"+pRef).addClass('active');
        $(".ifmPlayer-cont").slideDown();
    }
    
    if(pSect === "secao"){
        document.getElementById("ifmTxt").innerHTML = pDesc;
        
        var pos = $(".ifmPlayer-cont").position();
        
        $('html, body').animate({scrollTop: pos.top - 100}, 1500);
    }
    
    return false;
};

/* -------------------------------------------------------------------------- */
/* Fancybox */
$(".view-iframe").fancybox({
    openEffect: 'fade',
    titleShow: true,
    openMethod:'changeIn',
    padding: 2,
    helpers: {
        overlay :  {
            locked : false // item n�o vai ao TOP quando clicado
        },
        title: {
            type: 'float',
            title   : true
        },
        thumbs: {
            width: 50,
            height: 50,
            position: 'top'
        }
    }
});


/* -------------------------------------------------------------------------- */

$('#fullpage').fullpage({
    anchors: ['Andre Valencio', 'Biografia', 'Instagram', 'Videos', 'Musicas', 'Contato'],
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: ['Andre Valencio', 'Biografia', 'Instagram', 'Videos', 'Musicas', 'Contato'],
    scrollingSpeed: 500,
    menu: '.menu1, #menu2',
    verticalCentered: false,
    css3: true,
    responsiveHeight: 800,
    responsiveWidth: 1024,
    autoScrolling: true,
    easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)',

    'afterLoad': function(anchorLink, index){

        if(anchorLink === 'Andre Valencio' ) {
            $("header").addClass('update');
        }else {
            $("header").removeClass('update');
        }

        if(anchorLink === 'Contato'){  

            $("footer").addClass('active');
        } else{
            $("footer").removeClass('active');
        }

    },    

    'afterResize': function(anchorLink, index){

    }
});


/* -------------------------------------------------------------------------- */
/* Facebook */
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
/* -------------------------------------------------------------------------- */
/* Finaliza��o e loadings */



/* Banner */

$('.banner-728x90').royalSlider({
    arrowsNav: false,
    loop: true,
    keyboardNavEnabled: true,
    controlsInside: true,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true,
    autoScaleSliderWidth: 728,
    autoScaleSliderHeight: 90,
    controlNavigation: 'bullets', 
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {
        enabled: true,
        pauseOnHover: false,
        stopAtAction: false,
        delay: 7000
    },
    transitionType: 'move', 
    easeInOut: true,
    globalCaption: false,
    deeplinking: {
        enabled: true,
        change: false
    },
    imgWidth: 728,
    imgHeight: 90
});

$('.banner-1366x768').royalSlider({
    arrowsNav: false,
    loop: true,
    keyboardNavEnabled: true,
    controlsInside: true,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true,
    autoScaleSliderWidth: 1366,
    autoScaleSliderHeight: 768,
    controlNavigation: 'bullets', 
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {
        enabled: true,
        pauseOnHover: false,
        stopAtAction: false,
        delay: 7000
    },
    transitionType: 'move', 
    easeInOut: true,
    globalCaption: false,
    deeplinking: {
        enabled: true,
        change: false
    },
    imgWidth: 1366,
    imgHeight: 768
});


/*
$("body").niceScroll({
    touchbehavior:false,
    autohidemode:false,
    hwacceleration: true,
    scrollspeed:80,
    cursorwidth:"7px",
    zindex:"999"
});
*/



var pageLoader = $.nPageLoader;
    pageLoader.init({});
    pageLoader.addEventListener( "progress", onProgress );
    pageLoader.addEventListener( "complete", onComplete );

    function onProgress( e ){
        $(".anm-sound .sd-branch .sd-stick .sd-load-bar").height(e.percent + "%");
    }

    function onComplete( e ){
        $(".anm-sound").fadeOut('slow'); 
        setTimeout(function(){

            $(".anm-sound").remove();
        },3000);
    }
    
                  