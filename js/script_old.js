/* -------------------------------------------------------------------------- */
/* Menu Marker */
var wdMark, goMark;
$('.branchMenu').mouseenter(function(){
    
    wdMark = $(this).width();
    goMark = $(this).position();

    $('#linkMarker').css({'width': wdMark, 'left': goMark.left});
    
});
/* -------------------------------------------------------------------------- */
/* Banner */
$('.banner-1920x900').royalSlider({
    arrowsNav: false,
    loop: true,
    keyboardNavEnabled: true,
    controlsInside: true,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true,
    autoScaleSliderWidth: 1920,
    autoScaleSliderHeight: 900,
    controlNavigation: 'bullets', 
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {
        enabled: true,
        pauseOnHover: false,
        stopAtAction: false,
        delay: 7000
    },
    transitionType: 'move', 
    easeInOut: true,
    globalCaption: false,
    deeplinking: {
        enabled: true,
        change: false
    },
    imgWidth: 1920,
    imgHeight: 900
});
$('.banner-728x90').royalSlider({
    arrowsNav: false,
    loop: true,
    keyboardNavEnabled: true,
    controlsInside: true,
    imageScaleMode: 'fill',
    arrowsNavAutoHide: false,
    autoScaleSlider: true,
    autoScaleSliderWidth: 728,
    autoScaleSliderHeight: 90,
    controlNavigation: 'bullets', 
    thumbsFitInViewport: false,
    navigateByClick: true,
    startSlideId: 0,
    autoPlay: {
        enabled: true,
        pauseOnHover: false,
        stopAtAction: false,
        delay: 7000
    },
    transitionType: 'move', 
    easeInOut: true,
    globalCaption: false,
    deeplinking: {
        enabled: true,
        change: false
    },
    imgWidth: 728,
    imgHeight: 90
});
/* -------------------------------------------------------------------------- */
$("#btnScrollTop").click(function() {
    $('html, body').animate({scrollTop: 0}, 1500);
});

$('[class*="effRipple"]').materialripple();

$('.btnActv').click(function(e) {
    e.preventDefault();
    $(this).addClass('active');
});
/* -------------------------------------------------------------------------- */
/*
$("#aside-toggle").click(function(){
    $("#aside-toggle, #aside-toggle .effToggle, #aside-evolve").toggleClass('active');
    $("#header-lg").toggleClass('opct0');
    $("body").toggleClass('blocked');
});
*/
$("#hd-toggle").click(function(){
    $("header, #hd-toggle .effToggle, #header-acd-content").toggleClass('active');
    $("#header-acd-menu").removeClass('starter');
    //$("#header-acd-content").slideToggle();
    $("body").toggleClass('blocked');

});

$(window).scroll(function(){
    var h = $("header").height();
    var y = $(window).scrollTop();
    if (y > (h)){
        $("header, section").addClass('update');
        $("#header-acd-menu").addClass('starter');
    } else {
        $("header, section").removeClass('update');
    }
});

/* -------------------------------------------------------------------------- */
$(".btnModal, .wrapShadow, .closeModal").click(function(){
    
    var modal = $(this).data('target');
    if($(modal).hasClass('active')) {
        
        $(modal).fadeOut().removeClass('active');
        $("body").removeClass('modal-open');
    }else {
        $('.modal').fadeOut().removeClass('active');
        
        // Add active class to sct title
        $(modal).addClass('active').fadeIn();
        $("body").addClass('modal-open');
    }
});

/* -------------------------------------------------------------------------- */
/* Form */  

$("input.data").mask("99/99/9999");
$("input.cep").mask("99999-999");
$("input.n_casa").mask("9999999");
$("input.rg").mask("99.999.999.9");
$("input.fone").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');



/* -------------------------------------------------------------------------- */
/* Blinds */
$(".clickOff").bind("contextmenu", function(e) {
    e.preventDefault();
});

$("a.linkNull").click(function() {
    return false;
});

/* -------------------------------------------------------------------------- */
function close_accordin_section() {
    $('a.acd-sct-title, a.acd-title-icon').removeClass('active');
    $('.acd-sct-content').slideUp(300).removeClass('open');
}
$('a.acd-sct-title, a.acd-title-icon').click(function(e) {
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');
    if($(e.target).is('.active')) {
        close_accordin_section();
    }else {
        close_accordin_section();
        // Add active class to sct title
        $(this).addClass('active');
        // Open up the hidden content panel
        $(".acd " + currentAttrValue).slideDown(300).addClass('open');
    }
    e.preventDefault();
});
/* -------------------------------------------------------------------------- */
/* slick 1 */
$('.view-slick1').slick({
    adaptiveHeight: true,
    speed: 500,
    dots: true,
    //autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1
});
$('.view-slick4').slick({
    infinite: true,
    adaptiveHeight: true,
    speed: 300,
    //autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 510,
        settings: {
          slidesToShow: 1
        }
      }
    ]
});


$('.view-slick5').slick({
    infinite: false,
    adaptiveHeight: true,
    speed: 300,
    autoplay: true,
    arrows: true,
    slidesToShow: 5,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 4
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1
        }
      }
    ]
});

/* -------------------------------------------------------------------------- */
/* fancybox 
$(".view-iframe").fancybox({
    openEffect: 'fade',
    titleShow: true,
    openMethod:'changeIn',
    padding: 2,
    helpers: {
        overlay :  {
            locked : false // item n�o vai ao TOP quando clicado
        },
        title: {
            type: 'float',
            title   : true
        },
        thumbs: {
            width: 50,
            height: 50,
            position: 'top'
        }
    }
});

$(".pg-view-iframe").fancybox({
        //maxWidth	: 1260,
        //maxHeight	: 800,
        fitToView	: false,
        width       : '95%',
        height      : '95%',
        autoSize	: false,
        closeClick	: false,
        openEffect	: 'fade',
        closeEffect	: 'fade',
    iframe: {
        //scrolling : 'auto',
        preload   : true,
        //locked : false // item n�o vai ao TOP quando clicado
    },
    helpers: {
        overlay :  {
            locked : false // item n�o vai ao TOP quando clicado
        }
    }
});

$(".view-iframe-video").click(function() {
    $.fancybox({
        'padding': 0,
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'title': this.title,
        'width': 800,
        'height': 550,
        'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
        'type': 'swf',
        'swf': {
            'wmode'	: 'transparent',
            'allowfullscreen': 'true'
        },
        helpers: {
            overlay :  {
                locked : false // item n�o vai ao TOP quando clicado
            }
        }
    });
    return false;
});
/* -------------------------------------------------------------------------- */
$('#fullpage').fullpage({
        anchors: ['Piraju_Folia', 'A_Festa', 'Multimidia', 'Programacao'],
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: ['Piraju_Folia', 'A_Festa', 'Multimidia', 'Programacao'],
        scrollingSpeed: 500,
        menu: '.menu1, #menu2',
        verticalCentered: false,
        css3: true,
        responsiveHeight: 650,
        responsiveWidth: 1024,
        autoScrolling: true
        
    });
/* -------------------------------------------------------------------------- */
/* Facebook */
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
/* -------------------------------------------------------------------------- */
/* Finaliza��o e loadings */
$("body").niceScroll({
    touchbehavior:false,
    autohidemode:false,
    hwacceleration: true,
    scrollspeed:80,
    cursorwidth:"7px",
    zindex:"999"
});

