<?php
    $dadosSecao=$conn->query("SELECT DS_INFORMACAO FROM INFORMACAO WHERE ID_INFORMACAOLOCAL=1 AND ID_INFORMACAOCATEGORIA=5");
?>

<div class="wd-100 floatL">
    <div class="container">
        <div class="content">
            <div class="wd-100 floatL pdg15L">
                <h2 class="fSize52 cGray3 fLight personTitle tLeft"><span class="fNormal">M</span>�sicas</h2>
            </div>
            <?php
                if(!empty($dadosSecao[0]['DS_INFORMACAO'])){
            ?>   
                    <div class="default floatL wd-100 pdg15 fSize20 cGray3">
                        <?=stripslashes($dadosSecao[0]['DS_INFORMACAO'])?>
                    </div>
            <?php
                }
            ?>
            <div  id="pg-musicas" class="wd-100 floatL mgn20T">
                <?php
                    $sql = "SELECT * FROM DISCOGRAFIA WHERE ID_DISCOGRAFIACATEGORIA IN (1,2) AND BO_ATIVO='S' ORDER BY ID_DISCOGRAFIA DESC";

                    $dadosAlbum = $conn->query($sql);

                    $unLink =  "<a class='tb-col-child tb-child-link effRipple effShadow live-3d linkNull' href='#' alt='Invalido' title='Invalido' target='_blank' rel='nofollow'></a>";

                    if(count($dadosAlbum)>0){

                        for ($j = 0; $j < count($dadosAlbum); $j++) {

                            $sqlMusica = "SELECT * FROM DISCOGRAFIAMUSICA WHERE ID_DISCOGRAFIA=".$dadosAlbum[$j]['ID_DISCOGRAFIA'];
                            $dadosMusica = $conn->query($sqlMusica);

                            if(count($dadosMusica)>0){
                ?>
                                <div class="floatL wd-100 pdg8 mgn20B">
                                    <h2 class="floatL wd-100 cWhite fSize28 bgOpc-dark9 pdg10"><?=$dadosAlbum[$j]['NM_DISCO']?></h2>
                                    <div class="content pRelative min-hg-300p">
                                        <div class="wd-30 hg-100 floatL pAbsolute pLeft pTop tCenter pdg3T pdg3R sm-pdg5 md-pRelative md-wd-100 tCenter">
                                            <div class="capa_album dInlineB wd-100 hg-100 pRelative">
                                                <?php if(file_exists("./arquivos/capadisco/".$dadosAlbum[$j]['ID_DISCOGRAFIA'].".jpg")){ ?>
                                                    <div class="bgMask imgOn zInd9" style="background-image: url(/arquivos/capadisco/<?=$dadosAlbum[$j]['ID_DISCOGRAFIA']?>.jpg);"></div>
                                                <?php }else{ ?>
                                                    <img class="bgMask imgOff" alt="<?=$dadosAlbum[$j]['NM_DISCO']?>" />
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="wd-70 md-wd-100 tb-view floatR">
                                            <?php

                                                    for ($i = 0; $i < count($dadosMusica); $i++) {
                                            ?>
                                                        <div class='tb-line mgn3T bgOpc-dark9 pRelative sm-pdg0'>
                                                            <div class='tb-col wd-45 '>
                                                                <div class='tb-col-child fBold tLeft sm-tCenter'>
                                                                    <div class="pdg60L sm-pdg5 fNormal cWhite floatL wd-100">
                                                                        <span class="fSize22 floatL clearB" ><?=(strlen($dadosMusica[$i]['NM_MUSICA']) > 20 ? substr($dadosMusica[$i]['NM_MUSICA'], 0, 20)."..." : $dadosMusica[$i]['NM_MUSICA'])?></span>
                                                                        <span class="fSize14 floatL clearB" ><?=(strlen($dadosMusica[$i]['URL_LETRA']) > 20 ? substr($dadosMusica[$i]['URL_LETRA'], 0, 20)."..." : $dadosMusica[$i]['URL_LETRA'])?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <audio id="player_<?=$i?>" class="likePlayer" src="/musicas/<?=$dadosAlbum[$j]['PASTA'].'/'.$dadosMusica[$i]['NM_ARQUIVO']?>" type="audio/mp3" controls="controls"></audio>
                                                            <div class='floatL sm-wd-100 pAbsolute pRight pTop pBottom sm-pRelative'>
                                                                <a class="md-wd-100 hg-100 floatL tCenter effShadow-white effRipple pdg10 linkNull dMiddle op-live-3d sm-dInlineB" href="<?=(strlen($dadosMusica[$i]['NM_MUSICA']) > 30 ? substr($dadosMusica[$i]['NM_MUSICA'], 0, 30)."..." : $dadosMusica[$i]['NM_MUSICA'])?>" <?= "onclick='location.href=\"downloadArq.php?qt=".$dadosMusica[$i]['ID_MUSICA']."&gal=".$dadosMusica[$i]['ID_DISCOGRAFIA']."&arquivo=arquivos/musicas/{$dadosAlbum[$j]['PASTA']}/{$dadosMusica[$i]['NM_ARQUIVO']}\" '"?> target="_blank" rel="nofollow" alt="Fazer download" title="Fazer download">
                                                                    <div class="bgMask bgWhite opct1 zInd1 displayOff sm-displayOn"></div>
                                                                    <i class="fIcon-det-download dInlineB fSize28 cFirst zInd2 sm-pdg5B"></i>
                                                                    
                                                                    <div class="blop-down tCenter pdg3B">
                                                                        <span class="fSize12 dInlineB cWhite" ><?=$dadosMusica[$i]['QT_ACESSO']?></span>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                            <?php
                                                    }
                                            ?>
                                        </div>
                                    </div>    
                                </div>    
                <?php
                            }
                        }
                    }
                ?>
            </div>
            
        </div>
    </div>
</div>