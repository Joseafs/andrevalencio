<?php
    include_once("_cabecalho.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <!-- 19/09/2016 -->
        <title> <?=$config->getConfig(1)?> </title>

        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <meta name="keywords" content="<?=$config->getConfig(2)?>" />
        <meta name="Description" content="<?=$config->getConfig(3)?>" />
        <meta name="author" content="Uses - Design & Software - www.uses.com.br" />
        <meta name="robots" content="noindex, nofollow" />
        
        <meta property="og:title" content="<?=$config->getConfig(1)?>" />
        <meta property="og:url" content="http://<?=$_SERVER['HTTP_HOST']; ?>" />
        <meta property="og:site_name" content="<?=$config->getConfig(1)?>" />
        <meta property="og:image" content="http://<?=$_SERVER['HTTP_HOST']; ?>/img/topoLogo.png" />
        <meta property="og:description" content="<?=$config->getConfig(1)?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1 , minimum-scale=1 ,maximum-scale=1, user-scalable=no"> 
        
        <!-- Chrome, Firefox OS and Opera -->
        <meta name="theme-color" content="#191919">
        <!-- Windows Phone -->
        <meta name="msapplication-navbutton-color" content="#191919">
        <!-- iOS Safari -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#191919">
        
        <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <link type="text/css" rel="stylesheet" href="/source/helpers/jquery.fancybox-thumbs.css">
        <link type="text/css" rel="stylesheet" href="/source/jquery.fancybox.css">
        <link type="text/css" rel="stylesheet" href="/source/helpers/jquery.fancybox-buttons.css">
        
        <link type="text/css" rel="stylesheet" href="/css/royalslider.css">
        <link type="text/css" rel="stylesheet" href="/css/rs-minimal-white.css">
        <link type="text/css" rel="stylesheet" href="/css/slick.css">
        <link type="text/css" rel="stylesheet" href="/css/jquery.materialripple.css">
        <link type="text/css" rel="stylesheet" href="/css/jquery.fullPage.css">
        
        <link rel="stylesheet" href="https://f.vimeocdn.com/styles/css_opt/mobile/async.min.css" media="all">
        <link rel="stylesheet" href="https://f.vimeocdn.com/p/2.38.0/css/player.css" media="all">
            
	<link rel="stylesheet" href="/player/mediaelementplayer.min.css" />
        
        <link type="text/css" rel="stylesheet" href="/css/aparencia.css?<?=time();?>">
        
        <script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
        <!--
            <!-- ANALYTICS  
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-7493429-39']);
                _gaq.push(['_trackPageview']);

                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();

            </script>
        -->
    </head>
    <body >
        <div id="fb-root"></div>
        
        <div class='anm-sound zIndAll'>
            <div class='sd-branch'>
                <div class='sd-stick'>
                    <div class='sd-load-bar'></div>
                </div>
                <div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                    <div class='sd-note'></div>
                </div>
            </div>
        </div>

        
        <?php
            if(!file_exists($_GET["cod"].".php") || $_GET["cod"]=="" || is_numeric($_GET["cod"]) || $_GET["cod"]=="home"){
                $navSct= 'pg-home';
            }else{
                $navSct= 'pg-section';
            }
            $rfNav= $_GET['cod']; 
        ?>
        <div id="<?=$navSct?>" class="floatL pRelative wd-100 min-hg-100">
            <?php include_once 'topo.php'; ?>
            <section class="zInd2 floatL wd-100">
                <?php
                    if($navSct == 'pg-home'){
                        include_once ("home.php");
                    } else {
                ?>
                        <?php include_once($_GET["cod"].".php")?>
                <?php    
                    }
                ?>
            </section>

            <?php 
                include_once 'rodape.php';
            ?>
        </div>
        <script src="https://apis.google.com/js/platform.js" async defer>
            {lang: 'pt-BR'}
        </script>
        
        <script type="text/javascript" src="/player/mediaelement-and-player.min.js"></script>

        <script type="text/javascript" src="/js/jquery.maskedinput-1.3.1.min.js"></script>
        <script type="text/javascript" src="/js/jquery.royalslider.min.js"></script>
        <script type="text/javascript" src="/js/slick.js"></script>
        <script type="text/javascript" src="/js/jquery.materialripple.js"></script>
        <script type="text/javascript" src="/js/jquery.fullPage.min.js"></script>
        
        <script type="text/javascript" src="/source/jquery.fancybox.js"></script>
        <script type="text/javascript" src="/source/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script type="text/javascript" src="/source/helpers/jquery.fancybox-buttons.js"></script>
        
        <script type="text/javascript" src="/js/jquery.nPageLoader.js"></script>
        <script type="text/javascript" src="/js/script.js"></script>
 
        <script>
            $(window).on('resize load', function(){
                wdWidth = $(window).innerWidth();
                
                localizeMarker('<?=$rfNav?>');
            });
        </script>
        
        
        
    </body>
</html>
<?php
    $conn->desconectar();
?>
