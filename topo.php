<header class="zInd99 min-hg-80p live-3d pFixed">
    <div id="hd-content" class="hg-100 wd-100 live-3d zInd9">
        <a id="hd-toggle" class="effRippleDark ovflwH effShadow linkNull op-live-3d floatL pdg3 gt-displayOff" href="#Menu" title="Menu" alt="Menu">
            <div class="effToggle">
                <div class="has-toggle live-3d"></div>
                <div class="has-toggle live-3d"></div>
                <div class="has-toggle live-3d"></div>
            </div>
        </a>
        <div class=" ovflwA-6p wd-100 hg-100 floatL">
            <a id="hd-logo" class="wd-40 floatL pdg50R pAbsolute pLeft hg-100 dMiddle bgWhite tRight md-tCenter live-3d effRipple" href="http://<?=$_SERVER['HTTP_HOST']; ?>">
                <div class="wd-100 floatL">
                    <div class="wd-80 dInlineB">
                        <svg class="max-wd-100 floatR" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="400px" viewBox="0 0 780 90" enable-background="new 0 0 780 90" xml:space="preserve">
                            <g>
                                    <path d="M51.808,67.499H26.707c-2.7,6.4-6.201,14.801-8.4,20.602H7.405l28.102-70.905h7.4l28.102,70.905H60.208L51.808,67.499z
                                             M30.507,57.998h17.401l-8.601-24.701L30.507,57.998z"/>
                                    <path d="M74.205,88.101V17.996l7.8,0.1l34.102,49.304V18.096h10.001v70.005h-8L84.306,39.197v48.903H74.205z"/>
                                    <path d="M161.306,18.196c22.901,0,34.402,17.501,34.402,35.002c0,17.501-11.5,34.903-34.402,34.903h-25.102V18.196H161.306z
                                             M161.306,77.999c16.201,0,24.401-12.4,24.401-24.801c0-12.4-8.201-24.901-24.401-24.901h-15.001v49.703H161.306z"/>
                                    <path d="M215.003,52.598h11.801c8.5,0,12.701-6.101,12.701-12.201c0-6-4.2-12.101-12.701-12.101h-15.501v59.804h-10V18.096h25.501
                                            c15.201,0,22.801,11.201,22.801,22.301c0,9.901-6.1,19.301-18.101,21.401l22.102,26.302h-13.101l-25.501-30.702V52.598z"/>
                                    <path d="M264.102,77.999h32.502v10.102h-42.703V18.196h42.603v10.101h-32.402v19.701h31.702v10.201h-31.702V77.999z
                                             M269.802,15.096l7.2-10.701h13.601l-11.301,10.701H269.802z"/>
                                    <path d="M383.402,18.196h10.801L366.102,88.3h-7.4l-28.102-70.104h11l20.802,52.303L383.402,18.196z"/>
                                    <path d="M428.702,67.499h-25.102c-2.7,6.4-6.2,14.801-8.4,20.602h-10.9l28.102-70.905h7.4l28.102,70.905h-10.801L428.702,67.499z
                                             M407.4,57.998h17.401l-8.601-24.701L407.4,57.998z"/>
                                    <path d="M461.699,77.999h32.901v10.102h-43.002V18.196h10.101V77.999z"/>
                                    <path d="M508.698,77.999H541.2v10.102h-42.702V18.196h42.603v10.101h-32.402v19.701H540.4v10.201h-31.702V77.999z M505.299,14.795
                                            l11.3-10h8.601l11.001,10h-10.801l-4.8-3.9l-4.301,3.9H505.299z"/>
                                    <path d="M545.697,88.101V17.996l7.8,0.1L587.6,67.399V18.096h10v70.005h-8l-33.802-48.903v48.903H545.697z"/>
                                    <path d="M609.998,28.196c6.101-6.3,14.701-10.4,24.201-10.4c16.001,0,28.602,10,32.603,25.801h-10.001
                                            c-3.7-10.5-12.301-15.801-22.602-15.801c-6.7,0-12.801,2.9-17.201,7.5s-7.2,10.801-7.2,18.001c0,7,2.8,13.201,7.2,17.801
                                            c4.4,4.601,10.501,7.501,17.201,7.501c10.601,0,19.602-6.101,23.201-16.701h9.901C663.301,77.8,650.5,88.601,634.199,88.601
                                            c-9.5,0-18.101-4-24.201-10.4c-6.001-6.4-10.001-15.202-10.001-24.902C599.997,43.397,603.997,34.597,609.998,28.196z"/>
                                    <path d="M705.198,28.196h-13.001v49.803h13.001v10.102h-36.103V77.999h13.001V28.196h-13.001v-10h36.103V28.196z"/>
                                    <path d="M715.096,27.796c6.4-6.5,15.401-10.601,25.202-10.601c9.9,0,18.801,4.1,25.302,10.601c6.3,6.5,10.4,15.401,10.4,25.302
                                            c0,9.9-4.101,18.801-10.4,25.301C759.099,85,750.198,89,740.298,89c-9.801,0-18.802-4-25.202-10.601
                                            c-6.3-6.5-10.4-15.401-10.4-25.301C704.695,43.197,708.796,34.297,715.096,27.796z M758.398,34.997
                                            c-4.7-4.701-11.2-7.601-18.101-7.601s-13.301,2.9-17.901,7.601c-4.601,4.6-7.5,11-7.5,18.101c0,7.1,2.899,13.5,7.5,18.101
                                            c4.601,4.7,11.001,7.601,17.901,7.601s13.4-2.9,18.101-7.601c4.501-4.601,7.4-10.901,7.4-18.101
                                            C765.799,45.897,762.899,39.597,758.398,34.997z"/>
                            </g>
                        </svg>
                    </div>
                </div>
            </a>
            <div id="hd-menu" class="wd-60 floatL tCenter pAbsolute pRight hg-100 dMiddle">
                <div class="wd-100 floatL">
                    <div class="dInlineB">
                        <ul class="contMenu floatL fLight fSize24 tUppercase pRelative pdg10T">
                            <li id="rf-biografia" class="branchMenu"><a class="linkMenu op-live-3d effRipple cWhite dMiddle" title="Biografia" href="/biografia">Biografia</a></li>
                            <li id="rf-midia" class="branchMenu"><a class="linkMenu op-live-3d effRipple cWhite dMiddle" title="M�dia" href="/midia">M�dia</a></li>
                            <li id="rf-musicas" class="branchMenu"><a class="linkMenu op-live-3d effRipple cWhite dMiddle" title="M�sicas" href="/musica">M�sicas</a></li>
                            <li id="rf-contato" class="branchMenu"><a class="linkMenu op-live-3d effRipple cWhite dMiddle" title="Contato" href="/contato">Contato</a></li>

                            <div id="linkMarker" class="bgFirst md-displayOff"></div>
                       </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="bgMask wrapShadow bgOpc-dark6 gt-displayOff live-3d zInd9"></div>
