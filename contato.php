<?php
    if ($_POST['btnEnviar']=="Enviar") {

        $erro = "";

        if ($nome->isEmpty()) {
            $erro = "Preencha seu Nome";
        } elseif ($email->isEmpty() || !$email->isMail()) {
            $erro = "Informe seu E-mail";
        } elseif ($telefone->isEmpty()) {
            $erro = "Informe o Telefone";
        } elseif ($cidade->isEmpty()) {
            $erro = "Informe o Nome da Cidade";
        } elseif ($estado->isEmpty()) {
            $erro = "Informe o Estado";
        } elseif ($assunto->isEmpty()) {
            $erro = "Informe o Assunto";
        } elseif ($mensagem->isEmpty()) {
            $erro = "Informe uma mensagem";
        }

        if (empty($erro)) {
            $emailConteudo = $conn->query("SELECT * FROM CONTEUDOEMAIL WHERE ID_CONTEUDOEMAIL=1");
            $emailConteudo[0]['CONTEUDO'] = str_replace("%NOME%", $_POST['nome'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%EMAIL%", $_POST['email'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%TELEFONE%", $_POST['telefone'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%CIDADE%", $_POST['cidade'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%ESTADO%", $_POST['estado'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%ASSUNTO%", $_POST['assunto'], $emailConteudo[0]['CONTEUDO']);
            $emailConteudo[0]['CONTEUDO'] = str_replace("%MENSAGEM%", nl2br($_POST['mensagem']), $emailConteudo[0]['CONTEUDO']);

            $enviaremail = new Mail($config->getConfig(1)." - CONTATO VIA WEBSITE", $config->getConfig(12), $emailConteudo[0]['ASSUNTO'], $emailConteudo[0]['CONTEUDO']);
            $enviaremail->setReplyTo($_POST['nome'], $_POST['email']);
            $enviaremail->setTo($config->getConfig(1)." - CONTATO VIA WEBSITE", $config->getConfig(12));
            if ($enviaremail->enviar()) {
                $conn->beginTransaction();
                $sql = "INSERT INTO LOGCONTATO(ID_LOGCONTATO, NM_LOGCONTATO, EMAIL, TELEFONE, CIDADE, ESTADO, ASSUNTO, MENSAGEM) ".
                        "VALUES('',".$nome->getValorSql().",".$email->getValorSql().",".$telefone->getValorSql().",".$cidade->getValorSql().",".$estado->getValorSql().",".$assunto->getValorSql().",".$mensagem->getValorSql().") ";
                $conn->query($sql);
                $conn->endTransaction();

                $_POST = array();
                echo "<script>alert('Sua mensagem foi enviada com sucesso.\\n\\nEntraremos em contato o mais breve possivel.\\n\\nObrigado!')</script>";
            }else{
                echo "<script>alert('Erro no envio do contato.\\n\\nTente novamente!')</script>";
            }
        } else {
            echo "<script>alert('".$erro."')</script>";
        }
    }
    
    $dadosSecao=$conn->query("SELECT DS_INFORMACAO FROM INFORMACAO WHERE ID_INFORMACAOLOCAL=1 AND ID_INFORMACAOCATEGORIA=4");

?>

<div class="wd-100 floatL">
    <div class="container">
        <div class="content">
            <div class="wd-100 floatL pdg15L">
                <h2 class="fSize52 cGray3 fLight personTitle tLeft"><span class="fNormal">C</span>ontato</h2>
            </div>
            <?php
                if(!empty($dadosSecao[0]['DS_INFORMACAO'])){

                    $wdType="wd-50 sm-wd-100";
            ?>  
                    <div class="floatR wd-50 pdg20L sm-wd-100 mgn20B sm-pdg0">
                        <div class="default floatL wd-100 pdg8 cGray3">
                            <?=stripslashes($dadosSecao[0]['DS_INFORMACAO'])?>
                        </div>
                    </div>
            <?php
                }else{
                    $wdType="wd-100";
                }
            ?>
            <div class="floatL <?=$wdType?> pdg8">
                <form class="formInfo floatL wd-100 ipt-place-first" method="POST" action="" >
                    <div class="inputField wd-100 pdg3">
                        <input id="iptNome" type="text" placeholder="Nome" name="nome" class="selectField bgWhite bShwB pdg8" required>
                    </div>
                    <div class="inputField wd-60 pdg3">
                        <input id="iptEmail" type="email" placeholder="E-mail" name="email" class="selectField bgWhite bShwB pdg8" required>
                    </div>
                    <div class="inputField wd-40 pdg3">
                        <input id="iptFone" type="tel" placeholder="Telefone" name="telefone" class="selectField bgWhite fone bShwB pdg8" required>
                    </div>
                    <div class="inputField wd-60 pdg3">
                        <input id="iptCidade" type="text" placeholder="Cidade" name="cidade" class="selectField bgWhite bShwB pdg8" required>
                    </div>
                    <div class="inputField wd-40 pdg3">
                        <select id="sltEstado" name="estado" class="selectField bgWhite cGray9 bShwB pdg8" required>
                            <option value="" class="displayOff">Estado</option>
                            <?php
                                $estados = $conn->query("SELECT * FROM ESTADO ORDER BY DS_ESTADO");
                                for ($i = 0; $i < count($estados); $i++) {
                                    echo "<option value='".$estados[$i]['ID_ESTADO']."'>".$estados[$i]['DS_ESTADO']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="inputField wd-100 pdg3">
                        <input id="iptAssunto" type="text" placeholder="Assunto" name="assunto" class="selectField bgWhite bShwB pdg8" required>
                    </div>
                    <div class="inputField wd-100 pdg3 pRelative pdg60R" >
                        <textarea id="txtMessage" placeholder="Mensagem" name="mensagem" class="selectField bRad3L bgWhite bShwB pdg8"></textarea>
                        <button name="btnEnviar" value="Enviar" class="bgFirst bRad3R cWhite effRipple floatR effShadow pntPointer pRelative live-3d bShwB" style="width: 57px;" type="submit" >
                            <span class="pdg5 wd-100 floatL clearB txt14">Enviar</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>